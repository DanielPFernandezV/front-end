import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { PacienteService } from 'src/app/_service/paciente.service';
import { MedicoService } from 'src/app/_service/medico.service';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { Medico } from 'src/app/_model/medico';
import { Especialidad } from 'src/app/_model/especialidad';
import { Detalleconsulta } from 'src/app/_model/detalleConsulta';
import { ExamenService } from 'src/app/_service/examen.service';
import { Examen } from 'src/app/_model/examen';
import { Consulta } from 'src/app/_model/consulta';
import { ConsultaListaExamen } from 'src/app/_model/consultaListaExamen';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  pacientes:Paciente[]=[];
  medicos:Medico[]=[];
  especialidades:Especialidad[]=[];
  examenes:Examen[]=[];

  idPacienteSeleccionado:number;
  idEspecialidadSeleccionado:number;
  idMedicoSeleccionado:number;
  idExamenSeleccionado:number;
  fechaSeleccionada:Date;
  maxFecha: Date=new Date();

  diagnostico:string;
  tratamiento:string;
  mensaje: string;

  detalleConsulta: Detalleconsulta[]=[];

  examenesSeleccionados: Examen[]=[];

  constructor(private consultaService:ConsultaService,private pacienteService:PacienteService,
    private medicoService:MedicoService,private especialidadService:EspecialidadService,
    private examenService:ExamenService, private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.listarpacientes();
    this.listarespecialidades();
    this.listarmedicos();
    this.listarexamen();
  }
  listarpacientes(){
    this.pacienteService.listar().subscribe(data=>{
      this.pacientes=data;
    });
  }

  listarmedicos(){
    this.medicoService.listar().subscribe(data=>{
      this.medicos=data;
    });
  }

  listarespecialidades(){
    this.especialidadService.listar().subscribe(data=>{
      this.especialidades=data;
    });
  }

  listarexamen(){
    this.examenService.listar().subscribe(data=>{
      this.examenes=data;
    });
  }

  agregar(){
    if (this.diagnostico != null && this.tratamiento != null) {
      let det = new Detalleconsulta();
      det.diagnostico = this.diagnostico;
      det.tratamiento = this.tratamiento;
      this.detalleConsulta.push(det);

      this.diagnostico = null;
      this.tratamiento = null;
    } else {
      this.mensaje = `Debe agregar un diagnóstico y tramiento`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }

  }

  removerDiagnostico(index: number){
    this.detalleConsulta.splice(index,1);
  }

  agregarExamen(){
    if (this.idExamenSeleccionado > 0) {

      let cont = 0;
      for (let i = 0; i < this.examenesSeleccionados.length; i++) {
        let examen = this.examenesSeleccionados[i];
        if (examen.idExamen === this.idExamenSeleccionado) {
          cont++;
          break;
        }
      }

      if (cont > 0) {
        this.mensaje = 'El examen se encuentra en la lista';
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        let ex = new Examen();
        ex.idExamen = this.idExamenSeleccionado;
        this.examenService.listarExamenPorId(this.idExamenSeleccionado).subscribe(data => {
          ex.nombre = data.nombre;
          this.examenesSeleccionados.push(ex);
        });
      }
    } else {
      this.mensaje = `Debe agregar un examen`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }

  }

  removerExamen(index: number){
    this.examenesSeleccionados.splice(index,1);
  }

  aceptar(){
    let medico=new Medico();
    medico.idMedico=this.idMedicoSeleccionado;
    let paciente=new Paciente();
    paciente.idpaciente=this.idPacienteSeleccionado;
    let especialidad=new Especialidad();
    especialidad.idEspecialidad=this.idEspecialidadSeleccionado;

    let consulta=new Consulta();
    consulta.especialidad=especialidad;
    consulta.paciente = paciente;
    consulta.medico = medico;

    consulta.fecha = moment(this.fechaSeleccionada).toISOString();
    consulta.detalleConsulta = this.detalleConsulta;

    let consultaListaExamen = new ConsultaListaExamen();
    consultaListaExamen.consulta = consulta;
    consultaListaExamen.lstExamen = this.examenesSeleccionados;

    this.consultaService.registrar(consultaListaExamen).subscribe(() => {
      this.snackBar.open("Se registró", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);

    });

  }

  estadoBotonRegistrar() {
    return (this.detalleConsulta.length === 0 || this.idEspecialidadSeleccionado === 0 || this.idMedicoSeleccionado === 0 || this.idPacienteSeleccionado === 0);
  }
  limpiarControles() {
    this.detalleConsulta = [];
    this.examenesSeleccionados = [];
    this.diagnostico = '';
    this.tratamiento = '';
    this.idPacienteSeleccionado = 0;
    this.idEspecialidadSeleccionado = 0;
    this.idMedicoSeleccionado = 0;
    this.idExamenSeleccionado = 0;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje = '';
  }





}
