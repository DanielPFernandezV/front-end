import { Component, OnInit, ViewChild } from '@angular/core';
import { PacienteService } from 'src/app/_service/paciente.service';
import { Paciente } from 'src/app/_model/paciente';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {
  
  displayedColumns=['idpaciente','nombres','apellidos','acciones'];
  dataSource:MatTableDataSource<Paciente>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cantidad : number;

  constructor(private pacienteservice:PacienteService,private snackbar:MatSnackBar) { }

  ngOnInit() {
    this.listar();
    this.pacienteservice.pacienteCambio.subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.sort=this.sort;
      this.dataSource.paginator=this.paginator;
    });
    this.pacienteservice.mensajeCambio.subscribe(data=>{
      this.snackbar.open(data,'AVISO',{duration:2000});
    });

  }
  eliminar(idpaciente:number){
    this.pacienteservice.eliminar(idpaciente).subscribe(()=>{
      this.pacienteservice.listar().subscribe(data=>{
        this.pacienteservice.pacienteCambio.next(data);
        this.pacienteservice.mensajeCambio.next("SE ELIMINÓ");
      })
    });

  }

  listar(){
    /*this.pacienteservice.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.sort=this.sort;
      this.dataSource.paginator=this.paginator;
    });*/
    this.pacienteservice.listarPageable(0,10).subscribe((data:any)=>{
      let pacientes =data.content;
      this.cantidad = data.totalElements;
      this.dataSource=new MatTableDataSource(pacientes);
      this.dataSource.sort=this.sort;
    })
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue
  }

  motrarMas(e : any){
    this.pacienteservice.listarPageable(e.pageIndex , e.pageSize).subscribe((data:any)=>{
      let pacientes =data.content;
      this.cantidad = data.totalElements;
      this.dataSource=new MatTableDataSource(pacientes);
      this.dataSource.sort=this.sort;
    
  })
}

}
