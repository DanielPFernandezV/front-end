import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  paciente:Paciente;
  form:FormGroup;
  edicion:boolean;
  id:number;

  constructor(private pacienteservice:PacienteService,private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    this.paciente = new Paciente();
    this.form=new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl(''),
      'email': new FormControl('')

    });
    this.route.params.subscribe((params:Params)=>{
      this.id=params['id'];
      this.edicion=this.id!=null;

      this.initForm()
    });
  }

  initForm(){
    if(this.edicion){
      this.pacienteservice.listarporid(this.id).subscribe(data =>{
        this.form = new FormGroup({
          'id': new FormControl(data.idpaciente),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'dni': new FormControl(data.dni),
          'direccion': new FormControl(data.direccion),
          'telefono': new FormControl(data.telefono),
          'email': new FormControl(data.email)
        });

    });
  }
    else{

    }
  }
  

  operar(){
    this.paciente.idpaciente=this.form.value['id'];
    this.paciente.nombres=this.form.value['nombres'];
    this.paciente.apellidos=this.form.value['apellidos'];
    this.paciente.dni=this.form.value['dni'];
    this.paciente.telefono=this.form.value['telefono'];
    this.paciente.email=this.form.value['email'];
    this.paciente.direccion=this.form.value['direccion'];

    if(this.edicion){
      this.pacienteservice.modificar(this.paciente).subscribe(()=>{
        this.pacienteservice.listar().subscribe(data=>{
          this.pacienteservice.pacienteCambio.next(data);
          this.pacienteservice.mensajeCambio.next("SE MODIFICÓ");
        });
      });
    }else{   
    
    this.pacienteservice.registrar(this.paciente).subscribe(()=>{
      this.pacienteservice.listar().subscribe(data=>{
        this.pacienteservice.pacienteCambio.next(data);
        this.pacienteservice.mensajeCambio.next("SE REGISTRÓ");
      });
    });
  }
  this.router.navigate(['paciente']);
  }

}
