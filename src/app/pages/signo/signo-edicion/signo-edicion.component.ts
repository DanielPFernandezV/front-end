import { ActivatedRoute, Router, Params } from '@angular/router';
import { SignoService } from './../../../_service/Signo.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Signo } from './../../../_model/signo';
import { Component, OnInit } from '@angular/core';
import { PacienteService } from 'src/app/_service/paciente.service';
import { Paciente } from 'src/app/_model/paciente';
import { MatSnackBar, MatDialog } from '@angular/material';
import * as moment from 'moment';
import { PacienteDialogoComponent } from './paciente-dialogo/paciente-dialogo.component';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  id: number;
  Signo: Signo;
  form: FormGroup;
  edicion: boolean = false;

  pacientes:Paciente[]=[];
  idPacienteSeleccionado:number;

  temperatura : string;
  pulso : string;
  ritmo : string;
  fechaSeleccionada:Date;

  constructor(private builder: FormBuilder,private signoService: SignoService,private pacienteService:PacienteService, private route: ActivatedRoute, private router: Router,
     private snackBar:MatSnackBar, private dialog: MatDialog) {
    this.Signo = new Signo();
    this.form = this.builder.group({
      'id': new FormControl(0),
      'paciente': new FormControl(''),
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'ritmo': new FormControl(''),
      'pulso': new FormControl('')
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
    this.listarpacientes();
  }

  initForm(){
    if (this.edicion) {
      this.signoService.listarporid(this.id).subscribe(data => {
        let id=data.idSigno;
        let paciente = data.paciente.nombres;
        let fecha = data.fecha;
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmo = data.ritmo;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente': new FormControl(paciente),
          'fecha': new FormControl(fecha),
          'temperatura': new FormControl(temperatura),
          'ritmo': new FormControl(ritmo),
          'pulso': new FormControl(pulso)
        });
      });
    }
  }

  listarpacientes(){
    this.pacienteService.listar().subscribe(data=>{
      this.pacientes=data;
    });
  }

  operar() {

    let signo = new Signo();
    let paciente=new Paciente();
    paciente.idpaciente=this.idPacienteSeleccionado;
    signo.idSigno = this.form.value['id'];
    signo.paciente = paciente;
    signo.fecha = moment(this.fechaSeleccionada).toISOString();
    signo.temperatura = this.temperatura;
    signo.ritmo = this.ritmo;
    signo.pulso = this.pulso;

    if (this.Signo != null && this.Signo.idSigno > 0) {
      this.signoService.modificar(signo).subscribe(data => {
        this.signoService.listar().subscribe(Signo => {
          this.signoService.signoCambio.next(Signo);
          this.signoService.mensajeCambio.next("Se modificó");
        });
      });
    } else {
      this.signoService.registrar(signo).subscribe(data => {
        console.log(data);
        this.signoService.listar().subscribe(Signo => {
          this.signoService.signoCambio.next(Signo);
          this.signoService.mensajeCambio.next("Se registró");
        });
      });
    }

    this.router.navigate(['Signo']);
  }
  openDialog(){
    
    this.dialog.open(PacienteDialogoComponent, {
      width: '250px'
    });
  }
}
