import { Paciente } from './paciente';

export class Signo{
    idSigno :number;
    paciente: Paciente;
    fecha :string;
    temperatura : string;
    ritmo: string;
    pulso: string;
}