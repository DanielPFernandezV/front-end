 export class Paciente{
     idpaciente:number;
     nombres: string;
     apellidos: string;
     dni: string;
     direccion: string;
     telefono: string;
     email: string;
 }