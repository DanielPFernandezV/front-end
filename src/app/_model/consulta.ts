import { Paciente } from './paciente';
import { Medico } from './medico';
import { Especialidad } from './especialidad';
import { Detalleconsulta } from './detalleConsulta';

export class Consulta{
    idConsulta :number;
    paciente: Paciente;
    fecha :string;
    medico : Medico;
    especialidad: Especialidad;
    detalleConsulta: Detalleconsulta[];
}