import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialComponent } from './pages/consulta/especial/especial.component';
import { BuscarComponent } from './pages/buscar/buscar.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './_service/guard.service';
import { Not401Component } from './pages/not401/not401.component';
import { RecuperarComponent } from './login/recuperar/recuperar.component';
import { TokenComponent } from './login/recuperar/token/token.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { SignoComponent } from './pages/signo/signo.component';
import { SignoEdicionComponent } from './pages/signo/signo-edicion/signo-edicion.component';

const routes: Routes = [
  {path:'paciente',component:PacienteComponent,children:[
    {path:'nuevo',component:PacienteEdicionComponent },
    {path:'edicion/:id',component:PacienteEdicionComponent }
  ], canActivate: [GuardService]},
  {path:'especialidad',component:EspecialidadComponent,children:[
    {path:'nuevo',component:EspecialidadEdicionComponent },
    {path:'edicion/:id',component:EspecialidadEdicionComponent }
  ], canActivate: [GuardService]},
  {path:'examen',component:ExamenComponent,children:[
    {path:'nuevo',component:ExamenEdicionComponent },
    {path:'edicion/:id',component:ExamenEdicionComponent }
  ], canActivate: [GuardService]},
  {path:'medico',component:MedicoComponent, canActivate: [GuardService]},
  {path:'consulta',component:ConsultaComponent, canActivate: [GuardService]},
  {path:'consulta-especial',component:EspecialComponent, canActivate: [GuardService]},
  {path:'buscar',component:BuscarComponent, canActivate: [GuardService]},
  {path:'reporte',component:ReporteComponent, canActivate: [GuardService]},
  {path:'perfil',component:PerfilComponent},
  {path:'signos',component:SignoComponent,children:[
    {path:'nuevo',component:SignoEdicionComponent },
    {path:'edicion/:id',component:SignoEdicionComponent }
  ], canActivate: [GuardService]},
  { path: 'not-401', component: Not401Component },
  {
    path: 'recuperar', component: RecuperarComponent, children: [
      { path: ':token', component: TokenComponent }
    ]
  },  

  
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] 
})
export class AppRoutingModule { }
