import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HOST } from '../_shared/var.constants';
import { Subject } from 'rxjs';
import { Signo } from '../_model/signo';

@Injectable({
  providedIn: 'root'
})
export class SignoService {

  url: string = HOST;
  signoCambio= new Subject<Signo[]>();
  mensajeCambio= new Subject<string>();
  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Signo[]>(`${this.url}/signos`);
  }
  
  listarPageable(p:number,s: number){
    return this.http.get(`${this.url}/signos/pageable?page=${p}&size=${s}`);

  }

  registrar(signo: Signo){
    return this.http.post(`${this.url}/signos`, signo);
  }
  modificar(signo: Signo){
    return this.http.put(`${this.url}/signos`,signo);
  }

  eliminar(idsigno:number){
    return this.http.delete(`${this.url}/signos/${idsigno}`);

  }

  listarporid(idsigno:number){
    return this.http.get<Signo>(`${this.url}/signos/${idsigno}`);
  }

}